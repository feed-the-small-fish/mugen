from typing import List

def get_numbers() -> List[int]:
    return [1, 2, 3, 4, 5]

if __name__ == "__main__":
    numbers = get_numbers()
    print("Numbers:", numbers)